Feature: the data can be handled by engine

  Scenario Outline: client makes call to handle multiple events
    When the client sends data
      | name    | index    | value    | description    |
      | <name1> | <index1> | <value1> | <description1> |
      | <name2> | <index2> | <value2> | <description2> |
      | <name3> | <index3> | <value3> | <description3> |
    Then the client receives status code of 200
    And the client receives handled data
      | name    | index    | value    | description    |
      | <name1> | <index1> | <value1> | <description1> |
      | <name2> | <index2> | <value2> | <description2> |
      | <name3> | <index3> | <value3> | <description3> |

    Examples:
      | name1  | index1 | value1 | description1  | name2  | index2 | value2 | description2  | name3  | index3 | value3 | description3  |
      | Name11 | 11     | 155.23 | Description11 | Name21 | 21     | 255.23 | Description21 | Name31 | 31     | 355.23 | Description31 |
      | Name12 | 12     | 155.24 | Description12 | Name22 | 22     | 255.24 | Description22 | Name32 | 32     | 355.24 | Description32 |
      | Name13 | 13     | 155.25 | Description13 | Name23 | 23     | 255.25 | Description23 | Name33 | 33     | 355.25 | Description33 |
      | Name14 | 14     | 155.26 | Description14 | Name24 | 24     | 255.26 | Description24 | Name34 | 34     | 355.26 | Description34 |
      | Name15 | 15     | 155.27 | Description15 | Name25 | 25     | 255.27 | Description25 | Name35 | 35     | 355.27 | Description35 |
      | Name16 | 16     | 155.28 | Description16 | Name26 | 26     | 255.28 | Description26 | Name36 | 36     | 355.28 | Description36 |
