Feature: the data can be handled by engine

  Scenario Outline: client makes call to handle single event
    When the client sends data
      | name   | index   | value   | description   |
      | <name> | <index> | <value> | <description> |
    Then the client receives status code of 200
    And the client receives handled data
      | name   | index   | value   | description   |
      | <name> | <index> | <value> | <description> |

    Examples:
      | name  | index | value | description  |
      | Name1 | 1     | 55.23 | Description1 |
      | Name2 | 2     | 55.24 | Description2 |
      | Name3 | 3     | 55.25 | Description3 |
      | Name4 | 4     | 55.26 | Description4 |
      | Name5 | 5     | 55.27 | Description5 |
      | Name6 | 6     | 55.28 | Description6 |



