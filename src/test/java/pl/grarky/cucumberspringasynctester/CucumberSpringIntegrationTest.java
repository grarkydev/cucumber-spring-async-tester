package pl.grarky.cucumberspringasynctester;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;

@SpringBootTest
@ContextConfiguration(classes = CucumberSpringAsyncTesterApplication.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application.yml")
public class CucumberSpringIntegrationTest {


}
