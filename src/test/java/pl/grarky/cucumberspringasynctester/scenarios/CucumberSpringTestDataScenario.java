package pl.grarky.cucumberspringasynctester.scenarios;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import pl.grarky.cucumberspringasynctester.CucumberSpringIntegrationTest;
import pl.grarky.cucumberspringasynctester.feign.ClientsProvider;
import pl.grarky.cucumberspringasynctester.kafka.DataKafkaConsumer;
import pl.grarky.cucumberspringasynctester.model.Data;

import java.util.List;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class CucumberSpringTestDataScenario extends CucumberSpringIntegrationTest {

    private final ClientsProvider clientsProvider;
    private final DataKafkaConsumer dataKafkaConsumer;

    private Integer returnedCode = null;

    @Autowired
    public CucumberSpringTestDataScenario(ClientsProvider clientsProvider, DataKafkaConsumer dataKafkaConsumer) {
        this.clientsProvider = clientsProvider;
        this.dataKafkaConsumer = dataKafkaConsumer;
    }

    @Before
    public void clearReceivedData() {
        dataKafkaConsumer.clearReceivedData();
    }

    @When("^the client sends data$")
    public void clientSendsData(List<Data> dataTable) {
        returnedCode =
                clientsProvider
                        .dataClient()
                        .handleData(dataTable)
                        .getStatusCodeValue();
    }

    @Then("^the client receives status code of (\\d+)$")
    public void clientReceivedStatusCodeOf(int statusCode) {
        assertThat(returnedCode).isEqualTo(statusCode);
    }

    @And("^the client receives handled data$")
    public void clientReceivedHandledData(List<Data> dataTable) {
        await()
                .atMost(3, SECONDS)
                .until(() -> dataKafkaConsumer.isReady(dataTable.size()));

        assertThat(dataKafkaConsumer.receivedData())
                .usingFieldByFieldElementComparator()
                .containsAll(dataTable);
    }

}
