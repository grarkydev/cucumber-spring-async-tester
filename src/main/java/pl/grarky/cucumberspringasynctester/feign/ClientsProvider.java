package pl.grarky.cucumberspringasynctester.feign;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder;
import org.springframework.stereotype.Component;

@Component
public class ClientsProvider {

    private final String backendAddress;

    public ClientsProvider(@Value("${feign.server.address}") String backendAddress) {
        this.backendAddress = backendAddress;
    }

    public DataClient dataClient() {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new ResponseEntityDecoder(new JacksonDecoder()))
                .target(DataClient.class, backendAddress);
    }

}
