package pl.grarky.cucumberspringasynctester.feign;

import feign.Headers;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import pl.grarky.cucumberspringasynctester.model.Data;

import java.util.List;

@FeignClient("data")
public interface DataClient {

    @Headers("Content-Type: application/json")
    @RequestLine("POST /data")
    ResponseEntity<Void> handleData(List<Data> data);

}
