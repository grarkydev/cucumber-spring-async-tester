package pl.grarky.cucumberspringasynctester.model;

import java.math.BigDecimal;

public class Data {

    private String name;
    private Integer index;
    private BigDecimal value;
    private String description;

    public Data() {
    }

    public Data(String name, Integer index, BigDecimal value, String description) {
        this.name = name;
        this.index = index;
        this.value = value;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Data{" +
                "name='" + name + '\'' +
                ", index=" + index +
                ", value=" + value +
                ", description='" + description + '\'' +
                '}';
    }

}

