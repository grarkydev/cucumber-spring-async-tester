package pl.grarky.cucumberspringasynctester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CucumberSpringAsyncTesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CucumberSpringAsyncTesterApplication.class, args);
	}
}
