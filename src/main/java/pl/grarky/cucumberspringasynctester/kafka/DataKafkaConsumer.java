package pl.grarky.cucumberspringasynctester.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import pl.grarky.cucumberspringasynctester.model.Data;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataKafkaConsumer {

    private List<Data> receivedData = new ArrayList<>();

    @KafkaListener(topics = "${kafka.topic.data}")
    public void receive(Data payload) {
        receivedData.add(payload);
    }

    public void clearReceivedData() {
        receivedData.clear();
    }

    public boolean isReady(int dataAmount) {
        return receivedData.size() >= dataAmount;
    }

    public List<Data> receivedData() {
        return receivedData;
    }

}
